package com.jjgarcia.examenjhonly.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jjgarcia.examenjhonly.R
import com.jjgarcia.examenjhonly.ViewModel.CursosViewModel
import com.jjgarcia.examenjhonly.model.CursoDetalle
import com.jjgarcia.examenjhonly.model.Cursos
import com.jjgarcia.examenjhonly.model.Matricula
import kotlinx.android.synthetic.main.fragment_matricula2.*

class matricula : Fragment() {

    private lateinit var viewmodelCursos : CursosViewModel
    var curso : Cursos? = null

    private var detallecursoObderve = Observer<CursoDetalle>{detalle ->
        txtdetallecurso.text = detalle.detalle
    }

    private var estado = Observer<String>{activo ->
        Log.d("Esta LLamando","hasta que mande matricula")
        if (activo == "1"){
            txtmatricula.text = "Activio"
        }else{
            txtmatricula.text = "Activio"
        }
    }

    private var matriculaOn = Observer<Matricula>{matricula ->
        Log.d("Esta LLamando","hasta que mande matricula")
        if (matricula.estado == "2"){
            Toast.makeText(context,"Se ah Matriculado",Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(context,"NO se ah Matriculado",Toast.LENGTH_SHORT).show()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            curso = matriculaArgs.fromBundle(it).curso
        }
        viewmodelCursos = ViewModelProvider(this).get(CursosViewModel::class.java)
        curso?.let {
            it.código?.let { it1 -> viewmodelCursos.getCursoDetalle(it1) }
            viewmodelCursos.cursoDetalle.observe(this,detallecursoObderve)
        }
        viewmodelCursos.getEstadoPooling()
        viewmodelCursos.estado.observe(this,estado)
        viewmodelCursos.matricula.observe(this,matriculaOn)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_matricula2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cursoname.text = curso?.curso
        btnMatricula.setOnClickListener {
            viewmodelCursos.setMatricula(txtmatricula.text.toString())
        }
    }

}
