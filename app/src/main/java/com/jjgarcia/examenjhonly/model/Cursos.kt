package com.jjgarcia.examenjhonly.model
import android.os.Parcel
import android.os.Parcelable

data class  Estado(
    val estado : String?
)

class RequestMAtricula (val user_id : String,val curso_codigo : String){

}

data class Matricula(
    val estado : String?,
    val curso: String

)

data class  CursoDetalle(
    val detalle : String?,
    val código : String?,
    val nombre : String?
)

data class Cursos (

    val curso: String?,
    val código: String?

): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(curso)
        parcel.writeString(código)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Cursos> {
        override fun createFromParcel(parcel: Parcel): Cursos {
            return Cursos(parcel)
        }

        override fun newArray(size: Int): Array<Cursos?> {
            return arrayOfNulls(size)
        }
    }
}